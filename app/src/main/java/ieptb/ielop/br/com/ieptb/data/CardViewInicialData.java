package ieptb.ielop.br.com.ieptb.data;

import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.model.CardViewInicial;


/**
 * Created by 04717299302 on 07/04/2016.
 */
public class CardViewInicialData {


    static int id[] = {1,2};
    static String nome[] = {"CONSULTA","CARTÓRIOS"};
    static String descricaoItem[] = {"Gratuita de Protestos","Participantes da Consulta"};
    static int icone[] = {R.drawable.search_button,R.drawable.cartorio_protestos};

    public static ArrayList<CardViewInicial> getCards() {

        ArrayList<CardViewInicial> cardViewInicials = new ArrayList<>();

        for (int i = 0; i < nome.length; i++) {

            CardViewInicial card = new CardViewInicial(id[i], nome[i],descricaoItem[i], icone[i]);
            cardViewInicials.add(card);
        }

        return cardViewInicials;
    }


}
