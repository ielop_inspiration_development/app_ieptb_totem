package ieptb.ielop.br.com.ieptb.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ieptb.ielop.br.com.ieptb.R;


/**
 * Created by 04717299302 on 25/05/2016.
 */
public class NadaConstaAdapter extends RecyclerView.Adapter<NadaConstaAdapter.ViewHolder> {

    @Override
    public NadaConstaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nada_consta, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NadaConstaAdapter.ViewHolder holder, int position) {
        TextView txtNada = holder.txtNadaConsta;
        txtNada.setText(R.string.nao_existe);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNadaConsta;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtNadaConsta = (TextView) itemView.findViewById(R.id.txt_ndConsta);
            itemView.setTag(itemView);
        }
    }

}
