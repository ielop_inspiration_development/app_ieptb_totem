package ieptb.ielop.br.com.ieptb.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.adapter.LocalidadeAdapter;
import ieptb.ielop.br.com.ieptb.data.BandeiraData;
import ieptb.ielop.br.com.ieptb.entity.Bandeira;
import ieptb.ielop.br.com.ieptb.model.Localidade;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class CarolselActivity extends AppCompatActivity {
		
		private static final String KEY_SENHA = "keySenha";
		private static final String KEY_ID_FIREBASE = "keyIDFirebase";
		private static final String LOG_TAG = "Totem Debug";
		Realm realm;
		CarouselView carouselView;
		String latestVersion = "";
		SharedPreferences sharedpreferences;
		private static final String PASSWORD = "ieptbma2017";
		public static final String MY_PREFERENCES = "MY_PREFERENCES";
		List<Localidade> localidades= new ArrayList<Localidade>();
		LocalidadeAdapter adapter;
		Localidade localidadeSelecionada;
		;
		
		int[] sampleImages = {
						R.drawable.tela_01,
						R.drawable.tela_02,
						R.drawable.tela_03
		};
		
		private ImageView toque;
		private Dialog dialog;
		private EditText edtSenha;
		private Spinner locaSpinner;
		private Button buttonConfirmaSenha;
		private Button buttonCancel;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				setContentView(R.layout.activity_carolsel);
				
				if (Build.VERSION.SDK_INT < 16) {
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
										WindowManager.LayoutParams.FLAG_FULLSCREEN);
				} else {
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
										WindowManager.LayoutParams.FLAG_FULLSCREEN);
				}
				
				toque = (ImageView) findViewById(R.id.toque);
				carouselView = (CarouselView) findViewById(R.id.carousel);
				carouselView.setPageCount(sampleImages.length);
				carouselView.setImageListener(imageListener);
				realm = Realm.getDefaultInstance();
				sharedpreferences = getSharedPreferences(MY_PREFERENCES,
								Context.MODE_PRIVATE);
				
				toque.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
								if (!sharedpreferences.getString(KEY_ID_FIREBASE, "").toString().isEmpty()) {
										startActivity(new Intent(CarolselActivity.this, ConsultaActivity.class));
								} else {
										initDialog();
								}
						}
				});
				
				insertFlags(realm);
				
		}
		
		private void insertFlags(Realm realm) {
				
				realm.beginTransaction();
				
				ArrayList<Bandeira> flags = BandeiraData.getBandeiras();
				for (Bandeira bandeiraData : flags) {
						
						realm.copyToRealmOrUpdate(bandeiraData);
				}
				realm.commitTransaction();
				
				RealmResults<Bandeira> results = realm.where(Bandeira.class).findAll();
				results.sort("bandeira", Sort.DESCENDING);
		}
		
		ImageListener imageListener = new ImageListener() {
				
				@Override
				public void setImageForPosition(int position, ImageView imageView) {
						imageView.setAdjustViewBounds(true);
						imageView.setScaleType(ImageView.ScaleType.FIT_XY);
						Picasso.with(CarolselActivity.this)
										.load(sampleImages[position])
										.fit()
										.centerInside()
										.into(imageView);
						
						imageView.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
										
										if (!sharedpreferences.getString(KEY_ID_FIREBASE, "").toString().isEmpty()) {
												startActivity(new Intent(CarolselActivity.this, ConsultaActivity.class));
										} else {
												initDialog();
												
												
										}
										
										
								}
						});
				}
		};
		
		public void initDialog() {
				
				dialog = new Dialog(CarolselActivity.this);
				
				dialog.setContentView(R.layout.dialog_senha);
				LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.dialog);
				
				// Custom Android Allert Dialog Title
				dialog.setTitle("Confirmação de Senha.");
				edtSenha = (EditText) dialog.findViewById(R.id.edtSenha);
				locaSpinner = (Spinner) dialog.findViewById(R.id.localidade_spinner);
				buttonConfirmaSenha = (Button) dialog.findViewById(R.id.buttonConfirmaSenha);
				buttonCancel = (Button) dialog.findViewById(R.id.buttonCancelarSenha);
				getLocalidades();
				
				buttonConfirmaSenha.setOnClickListener(new View.OnClickListener() {
						@RequiresApi(api = Build.VERSION_CODES.O)
						@Override
						public void onClick(View v) {
								if (edtSenha.getText().toString().isEmpty()) {
										edtSenha.setError("Senha vazia!");
								} else {
										
										if (!edtSenha.getText().toString().equals(PASSWORD)) {
												alertPermissaoSenha();
										} else {
												SharedPreferences.Editor editor = sharedpreferences.edit();
												editor.putString(KEY_ID_FIREBASE, localidadeSelecionada.getId());
												editor.commit();
												edtSenha.setText("");
												dialog.dismiss();
												startActivity(new Intent(CarolselActivity.this, ConsultaActivity.class));
												
												
												
										}
								}
						}
				});
				buttonCancel.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
								dialog.dismiss();
						}
				});
				dialog.show();
				
		}
		
		@Override
		protected void onDestroy() {
				super.onDestroy();
				realm.close();
		}
		
		public void alertPermissaoSenha() {
				AlertDialog.Builder builder = new AlertDialog.Builder(CarolselActivity.this);
				builder.setPositiveButton("Fechar", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
						}
				});
				builder.setMessage("Permissão de consulta negada!Por favor entre em contato com IEPTB-MA.\nContato(s):(98) 3304 8117 / (98) 3304 8119");
				AlertDialog dialog = builder.create();
				dialog.show();
				
		}
//    private class GetLatestVersion extends AsyncTask<String, String, String> {
//        String latestVersion;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                //It retrieves the latest version by scraping the content of current version from play store at runtime
//                String urlOfAppFromPlayStore = "https://play.google.com/store/apps/details?id=ieptb.ielop.br.com.ieptb";
//                Document doc = Jsoup.connect(urlOfAppFromPlayStore).get();
//                latestVersion = doc.getElementsByAttributeValue("itemprop","softwareVersion").first().text();
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//            return latestVersion;
//        }
//    }
//    private String getCurrentVersion(){
//        PackageManager pm = this.getPackageManager();
//        PackageInfo pInfo = null;
//
//        try {
//            pInfo =  pm.getPackageInfo(this.getPackageName(),0);
//
//        } catch (PackageManager.NameNotFoundException e1) {
//            e1.printStackTrace();
//        }
//        String currentVersion = pInfo.versionName;
//
//        return currentVersion;
//    }
		
		public void getLocalidades() {
				
				DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("localidade");
				ValueEventListener listener2 = new ValueEventListener() {
						@Override
						public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
								
								for (DataSnapshot aux : dataSnapshot.getChildren()) {
										Localidade localidade =  aux.getValue(Localidade.class);
										localidades.add(localidade);
								}
								locaSpinner.setAdapter(new LocalidadeAdapter(localidades, CarolselActivity.this));
								locaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
										@Override
										public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
												localidadeSelecionada = (Localidade) parent.getItemAtPosition(position);
										}
										
										@Override
										public void onNothingSelected(AdapterView<?> parent) {
										
										}
								});
								
							
								
								
						}
						
						@Override
						public void onCancelled(@NonNull DatabaseError databaseError) {
						
						}
						
						
				};
				databaseReference.addValueEventListener(listener2);
		}
		
}
