package ieptb.ielop.br.com.ieptb.application;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import ieptb.ielop.br.com.ieptb.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by 04717299302 on 18/05/2016.
 */
public class CustomApplication extends Application {

   


    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name("totemapp.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

    }
    
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    
    
}
