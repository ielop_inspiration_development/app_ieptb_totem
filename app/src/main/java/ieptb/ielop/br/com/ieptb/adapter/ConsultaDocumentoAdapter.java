package ieptb.ielop.br.com.ieptb.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.entity.DocumentoCartorio;

/**
 * Created by 04717299302 on 13/04/2016.
 */
public class ConsultaDocumentoAdapter extends RecyclerView.Adapter<ConsultaDocumentoAdapter.ViewHolder> {

    private ArrayList<DocumentoCartorio> documento;
    private Context ctx;
    public ConsultaDocumentoAdapter(ArrayList<DocumentoCartorio> documento,Context ctx) {
        this.documento = documento;
        this.ctx=ctx;
    }

    @Override
    public ConsultaDocumentoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_consulta_documentos, parent, false);
        applyFont(ctx,view, "fonts/futura.ttf");
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ConsultaDocumentoAdapter.ViewHolder holder, int position) {

        TextView txtCartorio = holder.txtCartorio;
        TextView txtEndereco = holder.txtEndereco;
        TextView txtTelefone = holder.txtTelefone;
        TextView txtEmail = holder.txtEmail;
        TextView txtCountProtesto = holder.txtCountProtesto;
        TextView txtCidade = holder.txtCidade;
        TextView txtQtdProtesto = holder.txtQtdProtesto;





        SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");

        if (documento != null) {
            txtCountProtesto.setText("CARTÓRIO:"+String.valueOf(position+1)+"/"+String.valueOf(documento.size()));
            txtCartorio.setText(documento.get(position).getNome());
            txtEndereco.setText(documento.get(position).getEndereco());
            txtTelefone.setText(documento.get(position).getTelefone());
            txtEmail.setText(spf.format(documento.get(position).getDtAtualizacao()));
            txtCidade.setText(documento.get(position).getCidade());
            txtQtdProtesto.setText(String.valueOf(documento.get(position).getQtdProtestos()));
        } else {

            txtCartorio.setText("Nada Consta");
        }

    }

    @Override
    public int getItemCount() {
        return documento.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtCartorio;
        TextView txtEndereco;
        TextView txtTelefone;
        TextView txtEmail;
        TextView txtCountProtesto;
        TextView txtCidade;
        TextView txtQtdProtesto;


        public ViewHolder(View itemView) {
            super(itemView);

            this.txtCartorio = (TextView) itemView.findViewById(R.id.txt_cartorio);
            this.txtEndereco = (TextView) itemView.findViewById(R.id.txt_endereco);
            this.txtTelefone = (TextView) itemView.findViewById(R.id.txt_telefone);
            this.txtEmail = (TextView) itemView.findViewById(R.id.txt_email);
            this.txtCountProtesto=(TextView) itemView.findViewById(R.id.count_protestos);
            this.txtCidade = (TextView) itemView.findViewById(R.id.txt_cidade);
            this.txtQtdProtesto = (TextView) itemView.findViewById(R.id.txtQtdProtesto);

            itemView.setTag(itemView);
        }
    }

    public static void applyFont(final Context context, final View root, final String fontName) {
        try {
            if (root instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) root;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    applyFont(context, viewGroup.getChildAt(i), fontName);
            } else if (root instanceof TextView)
                ((TextView) root).setTypeface(Typeface.createFromAsset(context.getAssets(), fontName));
        } catch (Exception e) {
            Log.e("debug", String.format("Error occured when trying to apply %s font for %s view", fontName, root));
            e.printStackTrace();
        }
    }
}
