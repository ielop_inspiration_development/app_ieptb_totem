package ieptb.ielop.br.com.ieptb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.model.Localidade;

public class LocalidadeAdapter extends BaseAdapter {
		
		List<Localidade> localidades;
		Context ctx;
		LayoutInflater layoutInflater;
		
		public LocalidadeAdapter(List<Localidade> localidades, Context ctx) {
				this.localidades = localidades;
				this.ctx = ctx;
				layoutInflater = (LayoutInflater.from(this.ctx));
		}
		
		@Override
		public int getCount() {
				return localidades.size();
		}
		
		@Override
		public Object getItem(int position) {
				return localidades.get(position);
		}
		
		@Override
		public long getItemId(int position) {
				return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
				convertView = layoutInflater.inflate(R.layout.item_spinner_localidade, null);
				TextView txtNomeLocalidade = (TextView) convertView.findViewById(R.id.txt_nome_localidade);
				txtNomeLocalidade.setText(localidades.get(position).getNomeLocalidade());
				return convertView;
		}
}
