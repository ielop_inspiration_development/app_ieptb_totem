package ieptb.ielop.br.com.ieptb.entity;

/**
 * Created by Saulo G BIttencourt on 07/04/2016.
 */
public class Cartorio {

    private int id;
    private String descricao;
    private String endereco;
    private String bairro;
    private String municipio;
    private String cep;
    private String telefone;
    private String email;
    private String ddtoUltimaAtualizacao;
    private Cidade cidade;

    public Cartorio(){

    }

    public Cartorio(int id, String descricao, String endereco, String bairro, String municipio, String cep, String telefone, String email, String ddtoUltimaAtualizacao, Cidade cidade) {
        this.id = id;
        this.descricao = descricao;
        this.endereco = endereco;
        this.bairro = bairro;
        this.municipio = municipio;
        this.cep = cep;
        this.telefone = telefone;
        this.email = email;
        this.ddtoUltimaAtualizacao = ddtoUltimaAtualizacao;
        this.cidade = cidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDdtoUltimaAtualizacao() {
        return ddtoUltimaAtualizacao;
    }

    public void setDdtoUltimaAtualizacao(String ddtoUltimaAtualizacao) {
        this.ddtoUltimaAtualizacao = ddtoUltimaAtualizacao;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
}
