package ieptb.ielop.br.com.ieptb.data;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParserException;

import java.text.ParseException;
import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.WS.AuthTransportSE;
import ieptb.ielop.br.com.ieptb.entity.Documento;
import ieptb.ielop.br.com.ieptb.entity.DocumentoCartorio;
import ieptb.ielop.br.com.ieptb.util.ClasseUtil;
import ieptb.ielop.br.com.ieptb.util.StringToXML;

public class ConsultaPessoaData {

    public Document consultaPessoa(int TIPO_PESSOA,String CPFCNPJ){
        String SOAP_ACTION = "urn:serverTabelionatos#consulta";



        String METHOD_NAME = "consulta";

        String NAMESPACE = "urn:serverTabelionatos";
        String URL = "http://www.ieptb.com.br/ws/serverTabelionatos.php?wsdl";
         // String URL = "http://177.126.160.150/ws/serverTabelionatos.php?wsdl";

        Document doc = null;

        final String USERNAME = "crama";
        final String PASSWORD = "4m5t6y";

         //  final String USERNAME = "ieptb";
          // final String PASSWORD = "tn2s6dp";


        try {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            request.addProperty("tipo_doc",TIPO_PESSOA);
            request.addProperty("documento",CPFCNPJ);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.setOutputSoapObject(request);
            soapEnvelope.dotNet = true;
            soapEnvelope.implicitTypes = true;

            AuthTransportSE transport = new AuthTransportSE(URL,USERNAME,PASSWORD);
            transport.debug = true;
            transport.call(SOAP_ACTION, soapEnvelope);

            String xmlRetorno = soapEnvelope.getResponse().toString();
            doc = StringToXML.convertStringToDocument(xmlRetorno);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        return doc;
    }

    public Documento carregaInfoConultaDoc(Document doc) throws ParseException {
        NodeList root = doc.getChildNodes();

        ArrayList<Documento> consultaDocList = new ArrayList<>();
        Node consultaNode = StringToXML.getNode("consulta", root);
        NodeList consultas = consultaNode.getChildNodes();

        Documento documento = new Documento();
        DocumentoCartorio documentoCartorio = null;
        ArrayList<DocumentoCartorio> documentoCartorios = null;
        documento.setUfSemResposta(new String[]{StringToXML.getNodeValue("ufsemresposta", consultas)});
      //  documento.setTipoDocumento(Integer.parseInt(StringToXML.getNodeValue("tipo_documento", consultas)));
        documento.setDocumento(StringToXML.getNodeValue("documento", consultas));
        documento.setDtConsulta(ClasseUtil.convertToDate(StringToXML.getNodeValue("data_consulta", consultas)));
        documento.setSituacao(StringToXML.getNodeValue("situacao", consultas));

        if (documento.getSituacao().equals("CONSTA")) {
            Node conteudoNode = StringToXML.getNode("conteudo", consultaNode.getChildNodes());
            NodeList conteudos = conteudoNode.getChildNodes();

            documentoCartorios = new ArrayList<>();



            for (int i = 0; i < conteudos.getLength(); i++) {

                if (conteudos.item(i).hasChildNodes()) {

                    Node no = conteudos.item(i);
                    NodeList cartorios = no.getChildNodes();


                    String strData=StringToXML.getNodeValue("dt_atualizacao", cartorios).equals("")?"2017-06-22":StringToXML.getNodeValue("dt_atualizacao", cartorios);
                    String strCodigoCartorio=StringToXML.getNodeValue("codigo_cartorio", cartorios).equals("")?"0":StringToXML.getNodeValue("codigo_cartorio", cartorios);
                    String strCodigoCidade=StringToXML.getNodeValue("codigo_cidade", cartorios).equals("")?"0":StringToXML.getNodeValue("codigo_cidade", cartorios);
                    documentoCartorio =
                            new DocumentoCartorio(
                                    Integer.parseInt(strCodigoCartorio),
                                    StringToXML.getNodeValue("nome", cartorios),
                                    StringToXML.getNodeValue("telefone", cartorios),
                                    StringToXML.getNodeValue("endereco", cartorios),
                                    Integer.parseInt(strCodigoCidade),
                                    StringToXML.getNodeValue("cidade", cartorios),
                                    ClasseUtil.convertToDate(strData),
                                    Integer.parseInt(StringToXML.getNodeValue("protestos", cartorios).equals("")?"0":StringToXML.getNodeValue("protestos", cartorios))
                            );
                    if(!strCodigoCidade.equals("0")&&documentoCartorio.getQtdProtestos()>0) {
                        documentoCartorios.add(documentoCartorio);
                    }
                }
            }
        } else {
            //Quando nada consta


        }
        documento.setDocumentoCartorios(documentoCartorios);
        return documento;
    }

}
