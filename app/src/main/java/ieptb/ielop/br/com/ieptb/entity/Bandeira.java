package ieptb.ielop.br.com.ieptb.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Saulo G BIttencourt on 18/05/2016.
 */
public class Bandeira  extends RealmObject {
    @PrimaryKey
    private int id;
    private String bandeira;
    private int icone;

    public Bandeira(int id, String bandeira, int icone) {
        this.id = id;
        this.bandeira = bandeira;
        this.icone = icone;
    }

    public Bandeira() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public int getIcone() {
        return icone;
    }

    public void setIcone(int icone) {
        this.icone = icone;
    }
}
