package ieptb.ielop.br.com.ieptb.util;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Saulo G BIttencourt on 12/04/2016.
 */
public class ClasseUtil {

    public static Date convertToDate(String receivedDate) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(receivedDate);
        return date;
    }
    public static String convertDateToString(Date receivedDate) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format(receivedDate);
        return date;
    }

    public boolean isInternetAvailable(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        if(!i.isConnectedOrConnecting())
            return  false;

        return true;
    }

    public static void replaceDefaultFont(Context ctx, String fontBeReplaced, String newFont){
        Typeface typeface = Typeface.createFromAsset(ctx.getAssets(),newFont);
        replaceFont(fontBeReplaced,typeface);
    }

    private static void replaceFont(String fontBeReplaced, Typeface typeface) {
        try {
            Field field = Typeface.class.getDeclaredField(fontBeReplaced);
            field.setAccessible(true);
            field.set(null,typeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static String getDataAtual() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        // OU
        SimpleDateFormat dateFormat_hora = new SimpleDateFormat("HH:mm:ss");

        Date data = new Date();

        Calendar  cal = Calendar.getInstance();
        cal.setTime(data);
        Date data_atual = cal.getTime();

        String data_completa = dateFormat.format(data_atual);
        String hora_atual = dateFormat_hora.format(data_atual);
        return  data_completa+" "+ hora_atual;
    }
}
