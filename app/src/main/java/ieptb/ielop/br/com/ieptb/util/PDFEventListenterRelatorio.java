package ieptb.ielop.br.com.ieptb.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEvent;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ieptb.ielop.br.com.ieptb.R;

/**
 * Created by 04717299302 on 09/06/2016.
 */
public class PDFEventListenterRelatorio implements PdfPageEvent {
    protected float tableHeight;
    PdfPTable pdfPTable ;
    PdfPTable pdfPTableText ;
    static String  PATH_IMAGE;
    protected PdfContentByte cb;
    Font ffont = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);
    Font fonteTitulo = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);
    Font fonteTituloVermelho = new Font(Font.FontFamily.UNDEFINED, 9, Font.BOLD);
    Font fonteRodapeVermelho = new Font(Font.FontFamily.UNDEFINED, 8, Font.BOLD);
    Font ffont3 = new Font(Font.FontFamily.UNDEFINED, 9, Font.BOLD);
    Font ffont2 = new Font(Font.FontFamily.UNDEFINED, 7, Font.UNDERLINE);
    Document document;
    protected BaseFont helv;
    static String CPFCNPJ;
    Context ctx;
    protected PdfTemplate total;
    public static int pageNumber = 1;

    public PDFEventListenterRelatorio(String CPFCNPJ,Context ctx) {

        this.CPFCNPJ=CPFCNPJ;
        this.ctx=ctx;
    }

    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(100, 100);
        try {
            helv = BaseFont.createFont(BaseFont.HELVETICA,
                    BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            throw new ExceptionConverter(e);
        }

    }

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        try {
            document=document;
            pdfPTable=getTableHeader();

            pdfPTable.writeSelectedRows(0, -1,
                    document.left(),
                    document.top()+42 ,
                    writer.getDirectContent());


            cb = writer.getDirectContent();
            cb.saveState();
            String text = "Página: " + writer.getPageNumber() + "/" + pageNumber;
            float textBase = document.top()+40;
            float textSize = helv.getWidthPoint(text, 8);
            float adjust = helv.getWidthPoint("0", -10);
            cb.beginText();
            cb.setFontAndSize(helv, 8);
            cb.setTextMatrix(document.right() - textSize - adjust, textBase);
            cb.showText(text);
            cb.endText();
            cb.addTemplate(total, document.right() - adjust , textBase);
            cb.restoreState();

            ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                    new Phrase("_________________________________________________________________________________________________________________________________",ffont),

                    document.leftMargin()-60,
                    document.top()-10, 0);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {

        ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                new Phrase("_______________________________________________________________________________________________________________________________________________________",ffont),

                document.leftMargin()-60,
                document.bottom()+25, 0);

        footerTable().writeSelectedRows(0, -1, 50, 74, writer.getDirectContent());



    }

    @Override
    public void onCloseDocument(PdfWriter writer, Document document) {
        total.beginText();
        total.setFontAndSize(helv, 8);
        total.setTextMatrix(0,0);
        total.showText(String.valueOf(writer.getPageNumber() -1));
        total.endText();

    }

    @Override
    public void onParagraph(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title) {

    }

    @Override
    public void onChapterEnd(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title) {

    }

    @Override
    public void onSectionEnd(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onGenericTag(PdfWriter writer, Document document, Rectangle rect, String text) {

    }


    private PdfPTable getTableHeader() throws IOException, DocumentException {


//        PdfPTable table = new PdfPTable(2);
//        table.setTotalWidth(523);
//
//        Bitmap bitmap = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.logo_azul);
//        String path=getPathImage(bitmap, ctx);
//
//        table.setWidths(new int[]{ 1,3});
//        Image img = Image.getInstance(path);
//        img.scaleAbsolute(30, 40);
//        PdfPCell cell = new PdfPCell(img, true);
//        cell.setBorder(PdfPCell.NO_BORDER);
//        table.addCell(cell);
//
//        fonteTituloVermelho.setColor(BaseColor.RED);
//        Phrase p2= new Phrase(CPFCNPJ,fonteTituloVermelho);



        PdfPTable table = new PdfPTable(2);
        table.setTotalWidth(523);

        Bitmap bitmap = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.logo_new);
        String path=getPathImage(bitmap, ctx);

        table.setWidths(new int[]{1,3});

        Image img = Image.getInstance(path);
        img.scaleAbsolute(50f, 50f);
        img.setAbsolutePosition(-10f, 10f);

        PdfPCell cell = new PdfPCell(img, true);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);

        fonteTituloVermelho.setColor(BaseColor.RED);
        Phrase p2= new Phrase(CPFCNPJ,fonteTituloVermelho);








        Phrase p1= new Phrase("CONSULTA NACIONAL DE PROTESTOS – CNP\n" +
                              "Pesquisa Retroativa a 5 anos  |  Data:"+ClasseUtil.getDataAtual().concat("\n")+
                              "CPF/CNPJ CONSULTADO: ",fonteTitulo);
        p1.add(p2);


        cell= new PdfPCell(p1);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);


        return table;
    }
    public float getTableHeight() {
        return tableHeight;
    }


    public PdfPTable footerTable(){

        PdfPTable table = new PdfPTable(1);
        table.setTotalWidth(523);

        Phrase phareDois = new Phrase("Rua XV de Novembro -n°184-4° Andar- São Paulo/SP,CPE:01.013-000,Tel.:(11)3112-0698/3112-0699", ffont);
        PdfPCell cell = new PdfPCell(phareDois);
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        Phrase phraseTres=new Phrase("Site: ",ffont);
        Anchor anchor = new Anchor(
                "http://www.protestodetitulos.org.br",ffont2);
        anchor.setReference(
                "http://www.protestodetitulos.org.br");

        phraseTres.add(anchor);

        cell = new PdfPCell(phraseTres);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);

        Phrase phraseQuatro=new Phrase("Para mais informações acesse: ",ffont);

        Anchor anchor2 = new Anchor(
                "http://www.pesquisaprotesto.com.br",ffont2);
        anchor2.setReference(
                "http://www.pesquisaprotesto.com.br");

        phraseQuatro.add(anchor2);

        cell = new PdfPCell(phraseQuatro);
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        fonteRodapeVermelho.setColor(BaseColor.RED);

        Phrase p1= new Phrase("IMPORTANTE:OS DADOS FORNECIDOS TÊM CARÁTER EXCLUSIVAMENTE INFORMATIVO, SEM VALIDADE DE CERTIDÃO.",fonteRodapeVermelho);
        cell= new PdfPCell(p1);
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        return  table;
    }

    public PdfPTable getTableHeaderText () throws DocumentException {

        PdfPTable table = new PdfPTable(1);
        table.setTotalWidth(523);

        table.setWidths(new int[]{ 1});
        PdfPCell cell = null;

        Phrase p2= new Phrase("O prazo para exclusão do registro de protesto da base é de 5 dias após o cancelamento do protesto no cartório.",ffont3);
        cell= new PdfPCell(p2);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        return  table;
    }
    private String getPathImage(Bitmap bitMap, Context ctx) {

        String fileName = "logo_azul.png";
        String mPath = null;
        try {

            FileOutputStream out1 = ctx.openFileOutput(fileName, Context.MODE_PRIVATE);
            bitMap.compress(Bitmap.CompressFormat.JPEG, 100, out1);
            out1.flush();
            out1.close();
            File f = ctx.getFileStreamPath(fileName);
            mPath = f.getAbsolutePath();
            Log.i("PATH image", mPath);

        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return mPath;
    }
}
