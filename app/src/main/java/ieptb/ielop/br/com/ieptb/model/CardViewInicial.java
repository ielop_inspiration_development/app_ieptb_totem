package ieptb.ielop.br.com.ieptb.model;

/**
 * Created by 04717299302 on 07/04/2016.
 */
public class CardViewInicial {
    private int id;
    private String nome;
    private int icone;
    private String descricaoItem;


    public CardViewInicial(int id, String nome,String descricaoItem, int icone) {
        this.id = id;
        this.nome = nome;
        this.icone = icone;
        this.descricaoItem=descricaoItem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIcone() {
        return icone;
    }

    public void setIcone(int icone) {
        this.icone = icone;
    }

    public String getDescricaoItem() {
        return descricaoItem;
    }

    public void setDescricaoItem(String descricaoItem) {
        this.descricaoItem = descricaoItem;
    }

}

