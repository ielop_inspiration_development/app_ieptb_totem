package ieptb.ielop.br.com.ieptb.model;

import java.io.Serializable;

public class Localidade implements Serializable {
		
		String id;
		
		String nomeLocalidade;
		
		long contador;
		long contadorEmail;
		
		int status;
		
		public String getId() {
				return id;
		}
		
		public void setId(String id) {
				this.id = id;
		}
		
		public String getNomeLocalidade() {
				return nomeLocalidade;
		}
		
		public void setNomeLocalidade(String nomeLocalidade) {
				this.nomeLocalidade = nomeLocalidade;
		}
		
		public long getContador() {
				return contador;
		}
		
		public void setContador(long contador) {
				this.contador = contador;
		}
		
		public long getContadorEmail() {
				return contadorEmail;
		}
		
		public void setContadorEmail(long contadorEmail) {
				this.contadorEmail = contadorEmail;
		}
		
		public int getStatus() {
				return status;
		}
		
		public void setStatus(int status) {
				this.status = status;
		}
}
