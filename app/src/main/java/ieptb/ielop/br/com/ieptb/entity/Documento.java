package ieptb.ielop.br.com.ieptb.entity;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Saulo G BIttencourt on 12/04/2016.
 */
public class Documento {
    private String[] ufSemResposta;
    private int tipoDocumento;
    private String documento;
    private Date dtConsulta;
    private String situacao;
    private ArrayList<DocumentoCartorio> documentoCartorios;

    public Documento(String[] ufSemResposta, int tipoDocumento, String documento, Date dtConsulta, String situacao, ArrayList<DocumentoCartorio> documentoCartorio) {
        this.ufSemResposta = ufSemResposta;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
        this.dtConsulta = dtConsulta;
        this.situacao = situacao;
        this.documentoCartorios = documentoCartorio;
    }

    public Documento() {
    }

    public String[] getUfSemResposta() {
        return ufSemResposta;
    }

    public void setUfSemResposta(String[] ufSemResposta) {
        this.ufSemResposta = ufSemResposta;
    }

    public int getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(int tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public Date getDtConsulta() {
        return dtConsulta;
    }

    public void setDtConsulta(Date dtConsulta) {
        this.dtConsulta = dtConsulta;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public ArrayList<DocumentoCartorio> getDocumentoCartorios() {
        return documentoCartorios;
    }

    public void setDocumentoCartorios(ArrayList<DocumentoCartorio> documentoCartorios) {
        this.documentoCartorios = documentoCartorios;
    }
}
