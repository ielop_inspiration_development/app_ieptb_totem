package ieptb.ielop.br.com.ieptb.entity;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by Saulo G BIttencourt on 12/04/2016.
 */
public class DocumentoCartorio extends RealmObject {

    private int codigoCartorio;
    private String nome;
    private String telefone;
    private String endereco;
    private int codigoCidade;
    private String cidade;
    private Date dtAtualizacao;
    private int qtdProtestos;

    public DocumentoCartorio(int codigoCartorio, String nome, String telefone, String endereco, int codigoCidade, String cidade, Date dtAtualizacao, int qtdProtestos) {
        this.codigoCartorio = codigoCartorio;
        this.nome = nome;
        this.telefone = telefone;
        this.endereco = endereco;
        this.codigoCidade = codigoCidade;
        this.cidade = cidade;
        this.dtAtualizacao = dtAtualizacao;
        this.qtdProtestos = qtdProtestos;
    }

    public DocumentoCartorio() {
    }

    public int getCodigoCartorio() {
        return codigoCartorio;
    }

    public void setCodigoCartorio(int codigoCartorio) {
        this.codigoCartorio = codigoCartorio;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getCodigoCidade() {
        return codigoCidade;
    }

    public void setCodigoCidade(int codigoCidade) {
        this.codigoCidade = codigoCidade;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public int getQtdProtestos() {
        return qtdProtestos;
    }

    public void setQtdProtestos(int qtdProtestos) {
        this.qtdProtestos = qtdProtestos;
    }
}
