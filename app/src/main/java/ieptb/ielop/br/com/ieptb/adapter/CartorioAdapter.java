package ieptb.ielop.br.com.ieptb.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.entity.Cartorio;


public class CartorioAdapter extends RecyclerView.Adapter<CartorioAdapter.ViewHolder> {

    private ArrayList<Cartorio> cartorioList;

    public CartorioAdapter(ArrayList<Cartorio> cartorioList) {
        this.cartorioList = cartorioList;

    }

    @Override
    public CartorioAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_consulta_cartorio,parent,false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CartorioAdapter.ViewHolder holder, int position) {
        TextView txtCartorio = holder.txtCartorio;
        TextView txtEndereco = holder.txtEndereco;
        TextView txtBairro = holder.txtBairro;
        TextView txtMunicipio = holder.txtMunicipio;
        TextView txtCep = holder.txtCep;
        TextView txtTelefone = holder.txtTelefone;
        TextView txtEmail = holder.txtEmail;

        txtCartorio.setText(cartorioList.get(position).getDescricao());
        txtEndereco.setText(cartorioList.get(position).getEndereco());
        txtBairro.setText(cartorioList.get(position).getBairro());
        txtMunicipio.setText(cartorioList.get(position).getMunicipio());
        txtCep.setText(cartorioList.get(position).getCep());
        txtTelefone.setText(cartorioList.get(position).getTelefone());
        txtEmail.setText(cartorioList.get(position).getEmail());
    }

    @Override
    public int getItemCount() {
        return cartorioList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtCartorio;
        TextView txtEndereco;
        TextView txtBairro;
        TextView txtMunicipio;
        TextView txtCep;
        TextView txtTelefone;
        TextView txtEmail;

        public ViewHolder(View itemView) {
            super(itemView);

            this.txtCartorio = (TextView) itemView.findViewById(R.id.txt_cartorio);
            this.txtEndereco = (TextView) itemView.findViewById(R.id.txt_endereco);
            this.txtBairro = (TextView) itemView.findViewById(R.id.txt_bairro);
            this.txtMunicipio = (TextView) itemView.findViewById(R.id.txt_municipio);
            this.txtCep = (TextView) itemView.findViewById(R.id.txt_cep);
            this.txtTelefone = (TextView) itemView.findViewById(R.id.txt_telefone);
            this.txtEmail = (TextView) itemView.findViewById(R.id.txt_email);

            itemView.setTag(itemView);
        }
    }

}
