package ieptb.ielop.br.com.ieptb.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import ieptb.ielop.br.com.ieptb.R;


/**
 * Created by Saulo G BIttencourt on 02/06/2016.
 */
@SuppressLint("AppCompatCustomView")
public class MyTextView extends  TextView {

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MyTextView(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs!=null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
            String minhaFonte = a.getString(R.styleable.MyTextView_minhaFonte);

            if (minhaFonte!=null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/"+minhaFonte);
                setTypeface(myTypeface);
            }

            a.recycle();
        }
    }
}
