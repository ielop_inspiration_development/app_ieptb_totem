package ieptb.ielop.br.com.ieptb.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.entity.Bandeira;

import static ieptb.ielop.br.com.ieptb.activity.ConsultaActivity.getCartorios;

/**
 * Created by Saulo G BIttencourt on 18/05/2016.
 */
public class BandeiraAdapter extends RecyclerView.Adapter<BandeiraAdapter.ViewHolder>  {

    private ArrayList<Bandeira> bandeiras;
    private SparseBooleanArray bandeiraSelecionada;
    private Context ctx;


    public BandeiraAdapter(ArrayList<Bandeira> bandeira, Context ctx) {
        if (bandeira == null) {
            throw new IllegalArgumentException("Devem existir dados no array de bandeiras");
        }
        this.bandeiras = bandeira;
        this.bandeiraSelecionada = new SparseBooleanArray();
        this.ctx = ctx;
    }

    @Override
    public BandeiraAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =null;
        ViewHolder viewHolder=null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bandeira, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BandeiraAdapter.ViewHolder holder, final int position) {
        ImageView imgBandeira = holder.imgbandeira;
        final TextView txtBandeira =holder.bandeira;
        final LinearLayout linearLayout=holder.layoutBandeira;

        imgBandeira.setImageResource(bandeiras.get(position).getIcone());
        txtBandeira.setText(bandeiras.get(position).getBandeira().toString().toUpperCase());

        final View view = holder.imgbandeira;
        final Animation animation = AnimationUtils.loadAnimation(ctx,R.anim.shake);
        imgBandeira.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   view.startAnimation(animation);
                   getCartorios(bandeiras.get(position).getId(),bandeiras.get(position).getBandeira());

            }
        });
        imgBandeira.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager remove_keyboard = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                remove_keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);

                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
    return bandeiras.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgbandeira;
        TextView  bandeira;
        LinearLayout layoutBandeira;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imgbandeira = (ImageView) itemView.findViewById(R.id.bandeira);
            this.bandeira = (TextView) itemView.findViewById(R.id.txtBandeira);
            this.layoutBandeira =(LinearLayout) itemView.findViewById(R.id.bandeiras);

            itemView.setTag(itemView);
        }


    }

}

