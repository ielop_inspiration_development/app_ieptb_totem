package ieptb.ielop.br.com.ieptb.data;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.WS.AuthTransportSE;
import ieptb.ielop.br.com.ieptb.entity.Cartorio;
import ieptb.ielop.br.com.ieptb.util.StringToXML;

/**
 * Created by Saulo G BIttencourt on 11/04/2016.
 */
public class CartorioData {

    public Document tabelionatos(String UF){
        String SOAP_ACTION = "urn:serverTabelionatos#cartorios_participantes";
        String METHOD_NAME = "cartorios_participantes";
        String NAMESPACE = "urn:serverTabelionatos";
        String URL = "http://177.126.160.150/ws/serverTabelionatos.php?wsdl";


        ArrayList<Cartorio> data = null;

        Document doc = null;




        final String USERNAME = "ieptb";
        final String PASSWORD = "tn2s6dp";

        try {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            request.addProperty("uf",UF);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.setOutputSoapObject(request);
            soapEnvelope.dotNet = true;
            soapEnvelope.implicitTypes = true;
            soapEnvelope.encodingStyle = "utf-8";

            AuthTransportSE transport = new AuthTransportSE(URL,USERNAME,PASSWORD);
            transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            transport.debug = true;
            transport.call(SOAP_ACTION, soapEnvelope);

            String xmlRetorno = soapEnvelope.getResponse().toString();

            doc = StringToXML.convertStringToDocument(xmlRetorno);


        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }



        return doc;
    }

    public ArrayList<Cartorio> carregaTabelionatoNaoParticipante(Document doc,String condicaoCidadeCartorio){


        NodeList root = doc.getChildNodes();
        Node cnp = StringToXML.getNode("cnp", root);

        Node estadoNode = StringToXML.getNode("estado", cnp.getChildNodes());
        String estado = StringToXML.getNodeAttr("uf", estadoNode);

        Node cidadeNaoParticipantesNode = StringToXML.getNode(condicaoCidadeCartorio, estadoNode.getChildNodes());
        NodeList cidadeNaoParticipantes = cidadeNaoParticipantesNode.getChildNodes();

        ArrayList<Cartorio> cartorioList = new ArrayList<>();

        for (int i = 1; i < cidadeNaoParticipantes.getLength(); i++) {

            try {



                Node no = cidadeNaoParticipantes.item(i);

                Node cartorioNode = StringToXML.getNode("cartorios", no.getChildNodes());

                NodeList cartorios = cartorioNode.getChildNodes();

                Cartorio cartorio = new Cartorio();

                cartorio.setId(Integer.parseInt(StringToXML.getNodeValue("cartorio", cartorios)));
                cartorio.setDescricao(new String(StringToXML.getNodeValue("descricao", cartorios).getBytes("ISO-8859-1")));
                cartorio.setEndereco(new String(StringToXML.getNodeValue("endereco", cartorios).getBytes("ISO-8859-1")));
                cartorio.setBairro(new String(StringToXML.getNodeValue("bairro", cartorios).getBytes("ISO-8859-1")));
                cartorio.setMunicipio(new String(StringToXML.getNodeValue("municipio", cartorios).getBytes("ISO-8859-1")));
                cartorio.setCep(StringToXML.getNodeValue("cep", cartorios));
                cartorio.setTelefone(StringToXML.getNodeValue("telefone", cartorios));
                cartorio.setEmail(StringToXML.getNodeValue("email", cartorios));
                cartorio.setDdtoUltimaAtualizacao(StringToXML.getNodeValue("dt_atu", cartorios));

                cartorioList.add(cartorio);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return cartorioList;
    }
}
