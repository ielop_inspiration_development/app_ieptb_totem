package ieptb.ielop.br.com.ieptb.activity;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.GmailScopes;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Document;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.adapter.BandeiraAdapter;
import ieptb.ielop.br.com.ieptb.adapter.ConsultaDocumentoAdapter;
import ieptb.ielop.br.com.ieptb.data.ConsultaPessoaData;
import ieptb.ielop.br.com.ieptb.entity.Bandeira;
import ieptb.ielop.br.com.ieptb.entity.Documento;
import ieptb.ielop.br.com.ieptb.entity.DocumentoCartorio;
import ieptb.ielop.br.com.ieptb.util.ClasseUtil;
import ieptb.ielop.br.com.ieptb.util.GeneratePDF;
import ieptb.ielop.br.com.ieptb.util.Mask;
import ieptb.ielop.br.com.ieptb.util.SendEmail;
import ieptb.ielop.br.com.ieptb.util.ValidadorCPFCNPJ;
import io.realm.Realm;
import io.realm.RealmResults;
import pub.devrel.easypermissions.EasyPermissions;

import static ieptb.ielop.br.com.ieptb.activity.CarolselActivity.MY_PREFERENCES;

/**
 * Created by 04717299302 on 07/04/2016.
 */

public class ConsultaActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
		
		int TIPO_PESSOA = 1;
		RadioGroup rdGrupoTipoPessoa;
		Dialog dialogPersonalizado;
		TextView txtSession;
		
		private TextWatcher cpfMask;
		private TextWatcher cnpjSufixMask;
		private TextWatcher cnpjPrefixMask;
		
		private Button btnConsultar;
		
		private static Button fab;
		private static RecyclerView.Adapter adapter;
		private static RecyclerView recyclerView;
		private static RecyclerView.Adapter adapterFlag;
		private static RecyclerView recyclerViewFlag;
		private RecyclerView.LayoutManager layoutManager;
		private RecyclerView.LayoutManager layoutManagerFlag;
		ConsultaPessoaData consultaPessoaData;
		Documento documentos;
		private Document document;
		Documento documento;
		ProgressDialog mProgress;
		
		TextView txtSelecioneEstado;
		
		static Context ctx;
		
		// Storage Permissions variables
		private static final int REQUEST_EXTERNAL_STORAGE = 1;
		private static String[] PERMISSIONS_STORAGE = {
						Manifest.permission.READ_EXTERNAL_STORAGE,
						Manifest.permission.WRITE_EXTERNAL_STORAGE
		};
		static final int REQUEST_ACCOUNT_PICKER = 1000;
		static final int REQUEST_AUTHORIZATION = 1001;
		static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
		static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
		private static final String PREF_ACCOUNT_NAME = "accountName";
		private static final String[] SCOPES = {GmailScopes.MAIL_GOOGLE_COM};
		private static String EMAIL = "";
		GoogleAccountCredential mCredential;
		
		String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
		
		private static LinearLayout txtEstados;
		private static LinearLayout lnMain;
		private static LinearLayout lnSelectEstado;
		private static CoordinatorLayout lnProtestos;
		
		private ImageView logo;
		private LinearLayout consulta;
		
		private EditText edtCPF;
		private EditText edtPrefixo;
		private EditText edtSufixo;
		
		private TextInputLayout tiedtCPFCNPJ;
		private TextInputLayout tiedtCNPJPrefixo;
		private TextInputLayout tiedtCNPJSufixo;
		
		Button btnVoltar;
		Button btnNovaConsulta;
		
		static TextView txtEstadoSelecionado;
		private RadioButton rdCPF;
		static TextView txtBandeira;
		
		private String numeroDocumentoSemFormatacao;
		
		CountDown countDown;
		
		static int REQUEST_WRITE_EXTERNAL = 200;
		Realm realm;
		static Documento documentosObtidos;
		//private Tracker mTracker;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				
				if (Build.VERSION.SDK_INT < 16) {
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
										WindowManager.LayoutParams.FLAG_FULLSCREEN);
				} else {
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
										WindowManager.LayoutParams.FLAG_FULLSCREEN);
						
				}
				
				setContentView(R.layout.consulta_activity_off);
				
				 ClasseUtil.replaceDefaultFont(this, "DEFAULT", "fonts/before.ttf");
				
				int SDK_INT = android.os.Build.VERSION.SDK_INT;
				realm = Realm.getDefaultInstance();
				
				if (SDK_INT > 8) {
						StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
										.permitAll().build();
						StrictMode.setThreadPolicy(policy);
				}
				
				if (shouldAskPermission()) {
						verifyStoragePermissions(ConsultaActivity.this);
				}
				ctx = ConsultaActivity.this;
				
				Toolbar mToolbar = (Toolbar) findViewById(R.id.tb_main);
				
				TextView txtToolBar = (TextView) findViewById(R.id.main_toolbar_title);
				
				txtToolBar.setText("PESQUISA GRATUITA DE PROTESTOS");
				setSupportActionBar(mToolbar);
				
				countDown = new CountDown(24 * 10000, 1000, this, ConsultaActivity.this); // 2 minuto de espera na tela, depois disso volta pro carousel
				countDown.start();
				
				fab = (Button) findViewById(R.id.fab);
				fab.setVisibility(View.INVISIBLE);
				fab.setOnClickListener(this);
				txtSession = (TextView) findViewById(R.id.txt_session);
				rdGrupoTipoPessoa = (RadioGroup) findViewById(R.id.grupo_tipo_pessoa);
				consulta = (LinearLayout) findViewById(R.id.consulta);
				txtSelecioneEstado = (TextView) findViewById(R.id.selecione);
				txtSelecioneEstado.setText("SELECIONE UM ESTADO");
				edtCPF = (EditText) findViewById(R.id.edtCPFCNPJ);
				edtPrefixo = (EditText) findViewById(R.id.edtCNPJPrefixo);
				edtSufixo = (EditText) findViewById(R.id.edtCNPJSufixo);
				tiedtCPFCNPJ = (TextInputLayout) findViewById(R.id.tiedtCPFCNPJ);
				tiedtCNPJPrefixo = (TextInputLayout) findViewById(R.id.tiedtCNPJPrefixo);
				tiedtCNPJSufixo = (TextInputLayout) findViewById(R.id.tiedtCNPJSufixo);
				btnVoltar = (Button) findViewById(R.id.btnVoltar);
				btnConsultar = (Button) findViewById(R.id.btn_consultar);
				btnNovaConsulta = (Button) findViewById(R.id.btnNovaConsulta);
				rdCPF = (RadioButton) findViewById(R.id.rdCPF);
				txtBandeira = (TextView) findViewById(R.id.txtBandeira);
				lnMain = (LinearLayout) findViewById(R.id.lnMain);
				
				btnVoltar.setOnClickListener(this);
				
				btnConsultar.setOnClickListener(this);
				btnNovaConsulta.setOnClickListener(this);
				
				lnMain.setOnTouchListener(new View.OnTouchListener() {
						
						@Override
						public boolean onTouch(View v, MotionEvent event) {
								
								InputMethodManager remove_keyboard = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
								remove_keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
								
								return false;
						}
				});
				
				edtPrefixo.addTextChangedListener(new TextWatcher() {
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count, int after) {
								// TODO Auto-generated method stub
								if (edtPrefixo.getText().toString().length() == 10) //size as per your requirement
								{
										edtSufixo.requestFocus();
								}
						}
						
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {
						
						}
						
						@Override
						public void afterTextChanged(Editable s) {
						
						}
				});
				
				txtEstadoSelecionado = (TextView) findViewById(R.id.listaPorotestos);
				
				recyclerView = (RecyclerView) findViewById(R.id.recycleview_consulta_documentos);
				recyclerViewFlag = (RecyclerView) findViewById(R.id.recycleview_bandeira_fragment);
				txtEstados = (LinearLayout) findViewById(R.id.selectEstado);
				lnProtestos = (CoordinatorLayout) findViewById(R.id.protestos);
				lnSelectEstado = (LinearLayout) findViewById(R.id.layout_flag);
				logo = (ImageView) findViewById(R.id.logo);
				lnMain = (LinearLayout) findViewById(R.id.lnMain);
				applyFont(this, lnMain, "fonts/futura.ttf");
				
				// Primeira animação.
				Animation animationTextTollBar = AnimationUtils.loadAnimation(this, R.anim.slide_top_to_bottom);
				consulta.setAnimation(animationTextTollBar);
				txtToolBar.setAnimation(animationTextTollBar);
				// Segunda animação.
				Animation animationLogo = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
				animationLogo.setStartOffset(200);
				logo.setAnimation(animationLogo);
				// Terceira animação.
				Animation animationRadio = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
				animationRadio.setStartOffset(1000);
				consulta.setAnimation(animationRadio);
				
				txtEstados.setVisibility(View.INVISIBLE);
				lnProtestos.setVisibility(View.INVISIBLE);
				applyFont(this, lnProtestos, "fonts/futura.ttf");
				
				layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
				recyclerView.setLayoutManager(layoutManager);
				
				cpfMask = Mask.insert("###.###.###-##", edtCPF);
				
				edtCPF.addTextChangedListener(cpfMask);
				
				cnpjSufixMask = Mask.insert("####-##", edtSufixo);
				edtSufixo.addTextChangedListener(cnpjSufixMask);
				
				cnpjPrefixMask = Mask.insert("##.###.###/", edtPrefixo);
				edtPrefixo.addTextChangedListener(cnpjPrefixMask);
				
				rdGrupoTipoPessoa.setOnCheckedChangeListener(this);
				
				edtCPF.setOnEditorActionListener(new TextView.OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
								if (event != null && KeyEvent.KEYCODE_ENTER == event.getKeyCode() || actionId == EditorInfo.IME_ACTION_SEARCH) {
										((InputMethodManager) ConsultaActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
														edtCPF.getWindowToken(), 0);
										
										lnProtestos.setVisibility(View.INVISIBLE);
										recyclerView.setVisibility(View.INVISIBLE);
										
										ClasseUtil classeUtil = new ClasseUtil();
										
										if (classeUtil.isInternetAvailable(ConsultaActivity.this)) {
												confirmAction();
										} else {
												
												lnSelectEstado.setVisibility(View.INVISIBLE);
												Context context = getApplicationContext();
												LayoutInflater inflater = getLayoutInflater();
												
												View enviarEmailOK = inflater.inflate(R.layout.sem_conexao, null);
												Toast toast = new Toast(context);
												
												toast.setView(enviarEmailOK);
												toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 0);
												toast.setDuration(Toast.LENGTH_LONG);
												toast.show();
										}
										return true;
								}
								return false;
						}
				});
				
				edtPrefixo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
								if (event != null && KeyEvent.KEYCODE_ENTER == event.getKeyCode() || actionId == EditorInfo.IME_ACTION_SEARCH) {
										((InputMethodManager) ConsultaActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
														edtPrefixo.getWindowToken(), 0);
										
										lnProtestos.setVisibility(View.INVISIBLE);
										recyclerView.setVisibility(View.INVISIBLE);
										
										ClasseUtil classeUtil = new ClasseUtil();
										
										if (classeUtil.isInternetAvailable(ConsultaActivity.this)) {
												confirmAction();
										} else {
												lnSelectEstado.setVisibility(View.INVISIBLE);
												Context context = getApplicationContext();
												LayoutInflater inflater = getLayoutInflater();
												
												View enviarEmailOK = inflater.inflate(R.layout.sem_conexao, null);
												Toast toast = new Toast(context);
												
												toast.setView(enviarEmailOK);
												toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 0);
												toast.setDuration(Toast.LENGTH_LONG);
												toast.show();
										}
										return true;
								}
								return false;
						}
				});
				
				edtSufixo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
								if (event != null && KeyEvent.KEYCODE_ENTER == event.getKeyCode() || actionId == EditorInfo.IME_ACTION_SEARCH) {
										((InputMethodManager) ConsultaActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
														edtPrefixo.getWindowToken(), 0);
										
										lnProtestos.setVisibility(View.INVISIBLE);
										recyclerView.setVisibility(View.INVISIBLE);
										
										ClasseUtil classeUtil = new ClasseUtil();
										
										if (classeUtil.isInternetAvailable(ConsultaActivity.this)) {
												confirmAction();
										} else {
												lnSelectEstado.setVisibility(View.INVISIBLE);
												Context context = getApplicationContext();
												LayoutInflater inflater = getLayoutInflater();
												
												View enviarEmailOK = inflater.inflate(R.layout.sem_conexao, null);
												Toast toast = new Toast(context);
												
												toast.setView(enviarEmailOK);
												toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 0);
												toast.setDuration(Toast.LENGTH_LONG);
												toast.show();
										}
										return true;
								}
								return false;
						}
				});
				
		}
		
		private ArrayList<Bandeira> getFlags(Realm realm) {
				RealmResults<Bandeira> results = realm.where(Bandeira.class).findAll();
				ArrayList<Bandeira> bandeiras = null;
				if (results.size() > 0) {
						bandeiras = new ArrayList<>();
						for (Bandeira bandeira : results) {
								bandeiras.add(bandeira);
								
						}
				}
				return bandeiras;
		}
		
		private void confirmAction() {
				boolean numeroDocumentoValido = false;
				numeroDocumentoSemFormatacao = "";
				if (!edtCPF.getText().toString().isEmpty() || !edtPrefixo.getText().toString().isEmpty()) {
						if (edtCPF.getText().toString().isEmpty()) {
								if (!edtPrefixo.getText().toString().isEmpty()) {
										
										if (!edtSufixo.getText().toString().isEmpty()) {
												
												numeroDocumentoSemFormatacao = retiraMask(edtPrefixo.getText().toString() + edtSufixo.getText().toString());
												if (ValidadorCPFCNPJ.validateCNPJ(numeroDocumentoSemFormatacao)) {
														numeroDocumentoValido = true;
														
												} else {
														edtPrefixo.setError("CNPJ inválido!!");
														edtPrefixo.setText("");
														edtSufixo.setText("");
														numeroDocumentoValido = false;
														
												}
										} else {
												numeroDocumentoSemFormatacao = retiraMask(edtPrefixo.getText().toString() + edtSufixo.getText().toString());
												numeroDocumentoValido = true;
										}
										
										
								}
						} else {
								numeroDocumentoSemFormatacao = retiraMask(edtCPF.getText().toString());
								if (ValidadorCPFCNPJ.validateCPF(numeroDocumentoSemFormatacao)) {
										numeroDocumentoValido = true;
										
								} else {
										edtCPF.setError("CPF inválido !!!");
										numeroDocumentoValido = false;
										edtCPF.setText("");
								}
								
						}
						
						if (numeroDocumentoValido) {
								
								SharedPreferences sharedpreferences;
								sharedpreferences = getSharedPreferences(MY_PREFERENCES,
												Context.MODE_PRIVATE);
								if (!sharedpreferences.getString("keyIDFirebase", "1").toString().isEmpty()) {
										final String keyID = sharedpreferences.getString("keyIDFirebase", "");
										final DatabaseReference databaseReference;
										databaseReference = FirebaseDatabase.getInstance().getReference().child("localidade");
										databaseReference.child(keyID).addListenerForSingleValueEvent(new ValueEventListener() {
												@Override
												public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
														long count = (long) dataSnapshot.child("contador").getValue();
														databaseReference.getDatabase().getReference().child("localidade").child(keyID).child("contador").setValue(count + 1);
														documento = new Documento();
														documento.setTipoDocumento(TIPO_PESSOA);
														documento.setDocumento(numeroDocumentoSemFormatacao);
														AsyncCallWS task = new AsyncCallWS(ConsultaActivity.this);
														task.execute();
												}
												
												@Override
												public void onCancelled(@NonNull DatabaseError databaseError) {
												
												}
										});
								}
								
						}
						
						
				} else {
						Context context = getApplicationContext();
						LayoutInflater inflater = getLayoutInflater();
						
						View enviarEmailOK = inflater.inflate(R.layout.digite_prefixo, null);
						Toast toast = new Toast(context);
						
						toast.setView(enviarEmailOK);
						toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 0);
						toast.setDuration(Toast.LENGTH_LONG);
						toast.show();
						
						edtCPF.setText("");
						edtPrefixo.setText("");
						edtSufixo.setText("");
						
						if (rdCPF.isChecked()) {
								edtCPF.requestFocus();
								InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
						} else {
								edtPrefixo.requestFocus();
								getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
								InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
						}
				}
				
		}
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
				boolean CPF, CNPJ;
				if (R.id.rdCPF == checkedId) {
						edtCPF.setText("");
						edtCPF.requestFocus();
						tiedtCPFCNPJ.setVisibility(View.VISIBLE);
						tiedtCNPJPrefixo.setVisibility(View.GONE);
						tiedtCNPJSufixo.setVisibility(View.GONE);
						edtPrefixo.setText("");
						edtSufixo.setText("");
						CPF = true;
						CNPJ = false;
						TIPO_PESSOA = 1;
				} else {
						edtPrefixo.setText("");
						edtSufixo.setText("");
						edtCPF.setText("");
						
						edtPrefixo.requestFocus();
						tiedtCPFCNPJ.setVisibility(View.GONE);
						tiedtCNPJPrefixo.setVisibility(View.VISIBLE);
						tiedtCNPJSufixo.setVisibility(View.VISIBLE);
						
						tiedtCNPJPrefixo.setFocusable(true);
						
						CPF = false;
						CNPJ = true;
						TIPO_PESSOA = 2;
				}
				
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
				int id = item.getItemId();
				
				switch (id) {
						// Id correspondente ao botÃ£o Up/Home da actionbar
						case android.R.id.home:
								NavUtils.navigateUpFromSameTask(this);
								break;
				}
				
				return super.onOptionsItemSelected(item);
		}
		
		private String retiraMask(String edtCPFCNPJ) {
				String CPFCNPJ = "";
				if (TIPO_PESSOA == 1) {
						CPFCNPJ = edtCPFCNPJ.replaceAll("-", "");
						String aux = CPFCNPJ.replace(".", "");
						CPFCNPJ = aux;
				} else {
						CPFCNPJ = edtCPFCNPJ.replaceAll("-", "");
						String aux = CPFCNPJ.replace("/", "");
						aux = aux.replace(".", "");
						aux = aux.replace("-", "");
						CPFCNPJ = aux;
						
				}
				return CPFCNPJ;
		}
		
		@Override
		public void onClick(View v) {
				switch (v.getId()) {
						case R.id.fab:
								initDialog();
								break;
						case R.id.btnVoltar:
								this.finish();
								break;
						case R.id.btn_consultar:
								btnConsultar.setVisibility(View.INVISIBLE);
								confirmAction();
								((InputMethodManager) ConsultaActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
												btnConsultar.getWindowToken(), 0);
								break;
						case R.id.btnNovaConsulta:
								Intent intent = getIntent();
								intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
								finish();
								startActivity(intent);
								break;
				}
				
		}
		
		private class AsyncCallWS extends AsyncTask<Void, Void, Documento> {
				Context ctx;
				boolean flag = false;
				
				@Override
				protected void onPreExecute() {
						mProgress = new ProgressDialog(ctx);
						mProgress.setMessage(" Aguarde realizando consulta ...");
						mProgress.show();
						
				}
				
				public AsyncCallWS(Context ctx) {
						this.ctx = ctx;
						
				}
				
				@Override
				protected Documento doInBackground(Void... params) {
						try {
								consultaPessoaData = new ConsultaPessoaData();
								
								document = consultaPessoaData.consultaPessoa(
												documento.getTipoDocumento(),
												documento.getDocumento());
								if (document != null) {
										documentos = consultaPessoaData.carregaInfoConultaDoc(document);
										flag = false;
										document = null;
										
								} else {
										
										ConsultaActivity.this.runOnUiThread(new Runnable() {
												public void run() {
														
														if (!verificaStatus()) {
																Toast.makeText(ConsultaActivity.this, "Por favor, verifique a conexão com a internet!", Toast.LENGTH_SHORT).show();
														} else {
																
																Toast.makeText(ConsultaActivity.this, "Erro ao tentar acessar servidor", Toast.LENGTH_SHORT).show();
														}
														
														flag = true;
												}
										});
										mProgress.dismiss();
								}
						} catch (ParseException e) {
								e.printStackTrace();
						}
						documentosObtidos = documentos;
						return documentos;
				}
				
				@Override
				protected void onPostExecute(Documento documento) {
						
						if (documento != null) {
								if (documentos.getDocumentoCartorios() != null) {
										GeneratePDF generatePDF = new GeneratePDF(ConsultaActivity.this, documento, readBandeiras(documento), 1, 0);
										generatePDF.gerar(numeroDocumentoSemFormatacao);
										
										txtEstados.setVisibility(View.VISIBLE);
										
										layoutManagerFlag = new LinearLayoutManager(ConsultaActivity.this, LinearLayoutManager.HORIZONTAL, false);
										recyclerViewFlag.setLayoutManager(layoutManagerFlag);
										lnSelectEstado.setVisibility(View.VISIBLE);
										lnSelectEstado.setOnTouchListener(new View.OnTouchListener() {
												
												@Override
												public boolean onTouch(View v, MotionEvent event) {
														
														InputMethodManager remove_keyboard = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
														remove_keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
														
														return false;
												}
										});
										
										txtEstados.setVisibility(View.VISIBLE);
										recyclerViewFlag.setVisibility(View.VISIBLE);
										fab.setVisibility(View.VISIBLE);
										
										adapterFlag = new BandeiraAdapter(readBandeiras(documento), ConsultaActivity.this);
										recyclerViewFlag.setAdapter(adapterFlag);
										
										RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
										itemAnimator.setAddDuration(1000);
										itemAnimator.setMoveDuration(5000);
										
										recyclerViewFlag.setOnTouchListener(new View.OnTouchListener() {
												
												@Override
												public boolean onTouch(View v, MotionEvent event) {
														
														InputMethodManager remove_keyboard = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
														remove_keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
														
														return false;
												}
										});
										
										recyclerViewFlag.setItemAnimator(itemAnimator);
										
								} else {
										txtEstados.setVisibility(View.INVISIBLE);
										lnProtestos.setVisibility(View.INVISIBLE);
										lnSelectEstado.setVisibility(View.INVISIBLE);
										
										lnMain = (LinearLayout) findViewById(R.id.lnMain);
										
										Context context = getApplicationContext();
										LayoutInflater inflater = getLayoutInflater();
										
										View enviarEmailOK = inflater.inflate(R.layout.sem_dados, null);
										Toast toast = new Toast(context);
										
										toast.setView(enviarEmailOK);
										toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 0);
										toast.setDuration(Toast.LENGTH_LONG);
										toast.show();
										
										edtCPF.setText("");
										edtPrefixo.setText("");
										edtSufixo.setText("");
										
										if (rdCPF.isChecked()) {
												edtCPF.requestFocus();
												InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
												imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
										} else {
												edtPrefixo.requestFocus();
												getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
												InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
												imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
										}
										
										context = getApplicationContext();
										inflater = getLayoutInflater();
										
										enviarEmailOK = inflater.inflate(R.layout.sem_dados, null);
										toast = new Toast(context);
										
										toast.setView(enviarEmailOK);
										toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 0);
										toast.setDuration(Toast.LENGTH_LONG);
										toast.show();
								}
								if (flag) {
										
										lnSelectEstado.setVisibility(View.INVISIBLE);
								}
								mProgress.dismiss();
						}
				}
		}
		
		private ArrayList<Bandeira> readBandeiras(Documento documento) {
				RealmResults<Bandeira> results = realm.where(Bandeira.class).findAll();
				ArrayList<Bandeira> bandeiras = new ArrayList<Bandeira>();
				Bandeira bandeira;
				int aux = 0;
				DocumentoCartorio documentoCartorio = null;
				String auxCodigoCidade;
				ArrayList<DocumentoCartorio> documentosCartorios = documento.getDocumentoCartorios();
				for (Bandeira flag : results) {
						for (int i = 0; i < documentosCartorios.size(); i++) {
								documentoCartorio = documentosCartorios.get(i);
								auxCodigoCidade = String.valueOf(documentoCartorio.getCodigoCidade()).substring(0, 2);
								if (flag.getId() == Integer.parseInt(auxCodigoCidade)) {
										aux++;
								}
						}
						if (aux != 0) {
								bandeiras.add(flag);
								aux = 0;
						}
						
				}
				
				return bandeiras;
				
		}
		
		private void initDialog() {
				dialogPersonalizado = new Dialog(ConsultaActivity.this);
				
				dialogPersonalizado.setContentView(R.layout.dialog_send_email);
				LinearLayout layout = (LinearLayout) dialogPersonalizado.findViewById(R.id.dialog);
				applyFont(ctx, layout, "fonts/futura.ttf");
				// Custom Android Allert Dialog Title
				dialogPersonalizado.setTitle("Confirmação de Email.");
				
				final EditText edtEmail = (EditText) dialogPersonalizado.findViewById(R.id.edtEmail);
				final EditText edtConfirmaEmail = (EditText) dialogPersonalizado.findViewById(R.id.edtConfirmaEmail);
				Button buttonSendEmail = (Button) dialogPersonalizado.findViewById(R.id.buttonSendEmail);
				Button buttonCancel = (Button) dialogPersonalizado.findViewById(R.id.buttonCancelar);
				dialogPersonalizado.show();
				
				countDown.cancel();
				countDown.start();
				
				buttonSendEmail.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
								
								if (edtEmail.getText().toString().isEmpty()) {
										edtEmail.setError("E-mail obrigatório!");
								} else {
										
										if (isEmailValid(edtEmail.getText().toString())) {
												alertaConfirmacao(edtEmail.getText().toString());
												
												
										} else {
												edtEmail.setError("E-mail inválido!");
												//edtConfirmaEmail.setText("");
												edtEmail.setText("");
												
										}
										
										//}
										
								}
						}
				});
				buttonCancel.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
								dialogPersonalizado.dismiss();
						}
				});
				dialogPersonalizado.show();
		}
		
		private void sendEmail() {
				
				if (!isDeviceOnline()) {
						mProgress.dismiss();
						Toast.makeText(ConsultaActivity.this, "Por favor , Verifique a sua conexÃ£o!", Toast.LENGTH_LONG).show();
				} else {
						
						SharedPreferences sharedpreferences;
						sharedpreferences = getSharedPreferences(MY_PREFERENCES,
										Context.MODE_PRIVATE);
						if (!sharedpreferences.getString("keyIDFirebase", "1").toString().isEmpty()) {
								final String keyID = sharedpreferences.getString("keyIDFirebase", "");
								final DatabaseReference databaseReference;
								databaseReference = FirebaseDatabase.getInstance().getReference().child("localidade");
								databaseReference.child(keyID).addListenerForSingleValueEvent(new ValueEventListener() {
										@Override
										public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
												long countEmail = (long) dataSnapshot.child("contadorEmail").getValue();
												databaseReference.getDatabase().getReference().child("localidade").child(keyID).child("contadorEmail").setValue(countEmail + 1);
												new MakeRequestTask(ConsultaActivity.this).execute();
										}
										
										@Override
										public void onCancelled(@NonNull DatabaseError databaseError) {
										
										}
								});
						}
						
						
				}
		}
		
		boolean isEmailValid(CharSequence email) {
				return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
		}
		
		@Override
		protected void onActivityResult(
						int requestCode, int resultCode, Intent data) {
				super.onActivityResult(requestCode, resultCode, data);
				switch (requestCode) {
						case REQUEST_GOOGLE_PLAY_SERVICES:
								if (resultCode != RESULT_OK) {
								
								} else {
										getResultsFromApi();
								}
								break;
						case REQUEST_ACCOUNT_PICKER:
								if (resultCode == RESULT_OK && data != null &&
												data.getExtras() != null) {
										String accountName =
														data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
										if (accountName != null) {
												SharedPreferences settings =
																getPreferences(Context.MODE_PRIVATE);
												SharedPreferences.Editor editor = settings.edit();
												editor.putString(PREF_ACCOUNT_NAME, accountName);
												editor.apply();
												mCredential.setSelectedAccountName(accountName);
												getResultsFromApi();
												
										}
								}
								break;
						case REQUEST_AUTHORIZATION:
								if (resultCode == RESULT_OK) {
										getResultsFromApi();
								}
								
								break;
						
				}
		}
		
		@Override
		public void onRequestPermissionsResult(int requestCode,
		                                       @NonNull String[] permissions,
		                                       @NonNull int[] grantResults) {
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				EasyPermissions.onRequestPermissionsResult(
								requestCode, permissions, grantResults, this);
				
		}
		
		private boolean isDeviceOnline() {
				ConnectivityManager connMgr =
								(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
				return (networkInfo != null && networkInfo.isConnected());
		}
		
		private boolean isGooglePlayServicesAvailable() {
				GoogleApiAvailability apiAvailability =
								GoogleApiAvailability.getInstance();
				final int connectionStatusCode =
								apiAvailability.isGooglePlayServicesAvailable(this);
				return connectionStatusCode == ConnectionResult.SUCCESS;
		}
		
		private void acquireGooglePlayServices() {
				GoogleApiAvailability apiAvailability =
								GoogleApiAvailability.getInstance();
				final int connectionStatusCode =
								apiAvailability.isGooglePlayServicesAvailable(this);
				if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
						showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
				}
		}
		
		void showGooglePlayServicesAvailabilityErrorDialog(
						final int connectionStatusCode) {
				GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
				Dialog dialog = apiAvailability.getErrorDialog(
								ConsultaActivity.this,
								connectionStatusCode,
								REQUEST_GOOGLE_PLAY_SERVICES);
				dialog.show();
		}
		
		private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
				
				Activity activity;
				
				public MakeRequestTask(Activity activity) {
						this.activity = activity;
				}
				
				@Override
				protected List<String> doInBackground(Void... params) {
						try {
								
								sentMessage();
								
						} catch (Exception e) {
								
								cancel(true);
								
						}
						return null;
				}
				
				@Override
				protected void onPreExecute() {
						mProgress.show();
				}
				
				@Override
				protected void onPostExecute(List<String> output) {
						mProgress.dismiss();
						
						Context context = getApplicationContext();
						LayoutInflater inflater = getLayoutInflater();
						
						View enviarEmailOK = inflater.inflate(R.layout.email_enviado, null);
						Toast toast = new Toast(context);
						
						toast.setView(enviarEmailOK);
						toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER, 0, 0);
						toast.setDuration(Toast.LENGTH_LONG);
						toast.show();
						activity.finish();
						
						
				}
				
				@Override
				protected void onCancelled() {
						mProgress.dismiss();
						
				}
		}
		
		private void getResultsFromApi() {
				if (!isGooglePlayServicesAvailable()) {
						acquireGooglePlayServices();
				} else if (mCredential.getSelectedAccountName() == null) {
						chooseAccount();
				} else if (!isDeviceOnline()) {
						mProgress.dismiss();
						Toast.makeText(ConsultaActivity.this, "Por favor , Verifique a sua conexÃ£o!", Toast.LENGTH_LONG).show();
						
						
				} else {
				
				}
		}
		
		private void chooseAccount() {
				if (EasyPermissions.hasPermissions(
								this, Manifest.permission.GET_ACCOUNTS)) {
						String accountName = getPreferences(Context.MODE_PRIVATE)
										.getString(PREF_ACCOUNT_NAME, null);
						if (accountName != null) {
								mCredential.setSelectedAccountName(accountName);
								//salva a conta do usuÃ¡rio
								getResultsFromApi();
						} else {
								// Start a dialog from which the user can choose an account
								startActivityForResult(
												mCredential.newChooseAccountIntent(),
												REQUEST_ACCOUNT_PICKER);
						}
				} else {
						// Request the GET_ACCOUNTS permission via a user dialog
						EasyPermissions.requestPermissions(
										this,
										"This app needs to access your Google account (via Contacts).",
										REQUEST_PERMISSION_GET_ACCOUNTS,
										Manifest.permission.GET_ACCOUNTS);
				}
		}
		
		public void sentMessage() {
				sendEmailMessage(Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/Toten BNP/PDFs GERADOS/" + "TotenAPP.pdf");
		}
		
		public static ArrayList<DocumentoCartorio> getCartorios(int codigoUF, String bandeira) {
				
				ArrayList<DocumentoCartorio> cartorios = new ArrayList<>();
				
				for (DocumentoCartorio aux : documentosObtidos.getDocumentoCartorios()) {
						String codigoUFCidade = String.valueOf(aux.getCodigoCidade());
						if (codigoUF == Integer.parseInt(codigoUFCidade.substring(0, 2))) {
								cartorios.add(aux);
						}
				}
				
				adapter = new ConsultaDocumentoAdapter(cartorios, ctx);
				recyclerView.setAdapter(adapter);
				
				lnProtestos.setVisibility(View.VISIBLE);
				txtEstadoSelecionado.setText("ESTADO SELECIONADO:");
				txtBandeira.setText(bandeira.toUpperCase());
				
				recyclerView.setVisibility(View.VISIBLE);
				return cartorios;
		}
		
		private boolean shouldAskPermission() {
				
				return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
				
		}
		
		public static void verifyStoragePermissions(Activity activity) {
				// Check if we have read or write permission
				int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
				int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
				
				if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
						// We don't have permission so prompt the user
						ActivityCompat.requestPermissions(
										activity,
										PERMISSIONS_STORAGE,
										REQUEST_EXTERNAL_STORAGE
						);
				}
		}
		
		@Override
		public boolean onTouchEvent(MotionEvent event) {
				boolean consumiu = super.onTouchEvent(event);
				int actionMasked = MotionEventCompat.getActionMasked(event);
				
				switch (actionMasked) {
						case MotionEvent.ACTION_DOWN:
								consumiu = true;
								countDown.cancel();
								countDown.start();
						case MotionEvent.ACTION_BUTTON_PRESS:
								consumiu = true;
								countDown.cancel();
								countDown.start();
						
				}
				return consumiu;
		}
		
		public static void applyFont(final Context context, final View root, final String fontName) {
				try {
						if (root instanceof ViewGroup) {
								ViewGroup viewGroup = (ViewGroup) root;
								for (int i = 0; i < viewGroup.getChildCount(); i++)
										applyFont(context, viewGroup.getChildAt(i), fontName);
						} else if (root instanceof TextView)
								
								((TextView) root).setTypeface(Typeface.createFromAsset(context.getAssets(), fontName));
				} catch (Exception e) {
						Log.e("debug", String.format("Error occured when trying to apply %s font for %s view", fontName, root));
						e.printStackTrace();
				}
		}
		
		public void alertaConfirmacao(final String email) {
				AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
				builder.setTitle("Confirmação de E-mail");
				builder.setMessage("Seu e-mail é: " + email);
				
				String positiveText = "SIM";
				builder.setPositiveButton(positiveText,
								new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
												// positive button logic
												mProgress = new ProgressDialog(ConsultaActivity.this);
												mProgress.setMessage("Enviando email  ...");
												EMAIL = email;
												sendEmail();
												dialog.dismiss();
												dialogPersonalizado.dismiss();
												
										}
								});
				
				String negativeText = "NÃO";
				builder.setNegativeButton(negativeText,
								new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
												// negative button logic
												dialog.dismiss();
												dialogPersonalizado.dismiss();
												
										}
								});
				
				AlertDialog dialog = builder.create();
				// display dialog
				dialog.show();
		}
		
		public class CountDown extends CountDownTimer {
				/**
				 * @param millisInFuture    The number of millis in the future from the call
				 * to {@link #start()} until the countdown is done and {@link #onFinish()}
				 * is called.
				 * @param countDownInterval The interval along the way to receive
				 * {@link #onTick(long)} callbacks.
				 */
				
				Context ctx;
				Activity activity;
				
				public CountDown(long millisInFuture, long countDownInterval, Context context, Activity activity) {
						super(millisInFuture, countDownInterval);
						this.ctx = context;
						this.activity = activity;
				}
				
				@Override
				public void onTick(long millisUntilFinished) {
						int progress = (int) (millisUntilFinished / 1000);
						txtSession.setText("Sessão expira em " + Integer.toString(progress) + "seg.");
						if (progress == 1) {
								ctx.startActivity(new Intent(ConsultaActivity.this, CarolselActivity.class));
								activity.finish();
						}
				}
				
				@Override
				public void onFinish() {
				
				}
		}
		
		public boolean verificaStatus() {
				boolean status = false;
				ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo netInfo = cm.getActiveNetworkInfo();
				if (netInfo != null) {
						switch (netInfo.getState()) {
								case CONNECTED:
										status = true;
										break;
								case CONNECTING:
										status = true;
										break;
								case DISCONNECTED:
										status = false;
										break;
								case DISCONNECTING:
										status = false;
										break;
								case SUSPENDED:
										status = false;
										break;
								case UNKNOWN:
										status = false;
										break;
						}
				} else {
						status = false;
				}
				return status;
		}
		
		@Override
		protected void onDestroy() {
				super.onDestroy();
				if (dialogPersonalizado != null && dialogPersonalizado.isShowing()) {
						dialogPersonalizado.dismiss();
				}
		}
		
		private void sendEmailMessage(final String pathArquivo) {
				
				SendEmail m = new SendEmail("cnp@ieptbma.com.br", "CRAMAieptb!2017protesto");
				String[] toArr = {EMAIL};
				m.setTo(toArr);
				
				m.setFrom("rafaelpinheiropereira@gmail.com");
				m.setSubject("Consulta Gratuita de Protestos MA");
				m.setBody("Em anexo, segue o informativo.");
				try {
						m.addAttachment(pathArquivo);//anexo opcional
						m.send();
				} catch (Exception e) {
						//tratar algum outro erro aqui
						Log.d("ERROR", e.toString());
				}
		}
}
