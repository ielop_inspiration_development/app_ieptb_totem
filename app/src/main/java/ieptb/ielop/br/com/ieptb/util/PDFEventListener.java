package ieptb.ielop.br.com.ieptb.util;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEvent;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * Created by 04717299302 on 26/05/2016.
 */
public class PDFEventListener implements PdfPageEvent {
    protected PdfTemplate total;
    protected BaseFont helv;
    protected PdfContentByte cb;
    Image imgLogo;



    public PDFEventListener(Image imgLogo) {
        this.imgLogo = imgLogo;
    }

    Font ffont = new Font(Font.FontFamily.UNDEFINED, 9, Font.NORMAL);
    Font ffont3 = new Font(Font.FontFamily.UNDEFINED, 6, Font.ITALIC);
    Font ffont2 = new Font(Font.FontFamily.UNDEFINED, 7, Font.UNDERLINE);


    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        total = writer.getDirectContent().createTemplate(100, 100);
        //total.setBoundingBox(new Rectangle(10, 60, 650,800));

        try {
            helv = BaseFont.createFont(BaseFont.HELVETICA,
                    BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        } catch (Exception e) {
            throw new ExceptionConverter(e);
        }

    }

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContent();
        Rectangle rect = new Rectangle(10, 30, 590,785);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(2);
        canvas.rectangle(rect);
        try {
            imgLogo.setAbsolutePosition(document.left()-35 , document.top()+4);
            imgLogo.setAlignment(Element.ALIGN_LEFT);
            writer.getDirectContent().addImage(imgLogo);



        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    @Override

    public void onEndPage(PdfWriter writer, Document document)

    {

        cb = writer.getDirectContent();
        cb.saveState();
        String text = "Página:" + writer.getPageNumber() + "/";
        float textBase = document.top();
        float textSize = helv.getWidthPoint(text, 8);
        float adjust = helv.getWidthPoint("0", 150);
        cb.beginText();
        cb.setFontAndSize(helv, 8);
        cb.setTextMatrix(document.right() - textSize - adjust, textBase);
        cb.showText(text);
        cb.endText();
        cb.addTemplate(total, document.right() - adjust , textBase);
        cb.restoreState();
        ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                new Phrase("INSTITUTO DE ESTUDOS DE PROTESTOS DE TITULOS DO BRASIL – IEPTB",ffont),

                document.leftMargin()+90,
                document.top()+40, 0);
        ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                new Phrase( "CONSULTA NACIONAL DE PROTESTO – CNP",ffont),
                document.leftMargin()+90,
                document.top()+28, 0);

        Phrase headerUM = new Phrase("Os dados fornecidos têm caráter exclusivamente informativo, sem validade de certidão.",ffont);
        ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                headerUM,
                document.leftMargin()+70,
                document.top()+18, 0);



        Phrase headerDois = new Phrase("O prazo para exclusão do registro de protesto da base é de 5 dias após o cancelamento do protesto no cartório.",ffont);

        ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                headerDois,
                document.leftMargin()+70,
                document.top()+10, 0);



        Phrase phraseUm = new Phrase("INSTITUTO DE ESTUDOS DE PROTESTOS DE TITULOS DO BRASIL-IEPTB/CRA-NACIONAL",ffont);
        Phrase phareDois = new Phrase("Rua XV de Novembro -n°184-4° Andar- São Paulo/SP,CPE:01.013-000,Tel.:(11)3112-0698/3112-0699", ffont);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                phraseUm,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.bottom() - 30, 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                phareDois,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.bottom() - 38, 0);
        Phrase phraseTres=new Phrase("Site: ",ffont);
        Anchor anchor = new Anchor(
                "http://www.protestodetitulos.org.br",ffont2);
        anchor.setReference(
                "http://www.protestodetitulos.org.br");

        phraseTres.add(anchor);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                phraseTres,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.bottom() - 48, 0);
        Phrase phraseQuatro=new Phrase("Pesquisa Gratuita de Protestos: ",ffont);


        Anchor anchor2 = new Anchor(
                "http://www.ieptb.com.br",ffont2);
        anchor2.setReference(
                "http://www.ieptb.com.br");

        phraseQuatro.add(anchor2);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                phraseQuatro,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.bottom() - 48, 0);

    }
    @Override
    public void onCloseDocument(PdfWriter writer, Document document) {
        total.beginText();
        total.setFontAndSize(helv, 8);
        total.setTextMatrix(0,0);
        total.showText(String.valueOf(writer.getPageNumber() -1));
        total.endText();
    }


    @Override
    public void onParagraph(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title) {

    }

    @Override
    public void onChapterEnd(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title) {

    }

    @Override
    public void onSectionEnd(PdfWriter writer, Document document, float paragraphPosition) {

    }

    @Override
    public void onGenericTag(PdfWriter writer, Document document, Rectangle rect, String text) {

    }
}
