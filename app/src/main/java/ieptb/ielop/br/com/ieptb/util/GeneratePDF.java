package ieptb.ielop.br.com.ieptb.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.entity.Bandeira;
import ieptb.ielop.br.com.ieptb.entity.Documento;
import ieptb.ielop.br.com.ieptb.entity.DocumentoCartorio;

/**
 * Created by 04717299302 on 16/04/2016.
 */
public class GeneratePDF {

    private static final String DOCUMENTO_PESQUISADO = "Documento Pesquisado:";
    private static final String ESTADO_CONSULTADO = "Estado(s) consultado(s):";
    private static final String PESQUISA_EFETUADA = "Pesquisa Efetuada em :";
    private static final String RETROATIVA = "Retroativa a cinco anos:";
    private static final String TITULO_IEPTB = "INSTITUTO DE ESTUDOS DE PROTESTO DE TÍTULOS DO BRASIL ";
    private static final String ASSERTS="assets/big.ttf";
    public static  Font NORMAL=null;

    Documento documento;
    Context ctx;
    ArrayList<Bandeira> bandeiras;
    Paragraph quebraLinha;
    Font quebraLinhaFonte;


    int tipoConsulta;
    int codigoCidade;


    public GeneratePDF(Context ctx, Documento documento, ArrayList<Bandeira> bandeiras, int tipoConsulta, int codigoCidade) {
        this.documento = documento;
        this.ctx = ctx;
        this.bandeiras=bandeiras;
        this.tipoConsulta = tipoConsulta;
        this.codigoCidade = codigoCidade;
    }

    public void gerar(String CPFCNPJ) {
        String pathPdf = Environment
                .getExternalStorageDirectory().getAbsolutePath().toString() + "/Toten BNP/PDFs GERADOS/";
        String pathFont = "assets/fonts/bfutura.ttf";
        String pathFont2 = "assets/fonts/big.ttf";
        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        try {
            File dir = new File(pathPdf);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(dir, "TotenAPP.pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfWriter writer = PdfWriter.getInstance(document, fOut);

            PDFEventListenterRelatorio pdfEventListenterRelatorio = new PDFEventListenterRelatorio(CPFCNPJ,ctx);
            writer.setPageEvent(pdfEventListenterRelatorio);

            document.open();
            document.add(new Paragraph(" "));

            BaseFont urName = BaseFont.createFont("assets/fonts/big.ttf", "",BaseFont.EMBEDDED);
            Font urFontName = new Font(urName, 12);

            montaListaCartorios(document,documento,urFontName,tipoConsulta, codigoCidade);

            document.close();

            Log.i("PDF", pathPdf);
        } catch (Exception e) {
            Log.e("Erro PDF", e.toString());
        }
    }





    public void  montaListaCartorios(Document document,Documento documento,Font big, int tipoConsulta, int codigoCidade) throws IOException, DocumentException, ParseException {
        quebraLinhaFonte = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
        quebraLinha=new Paragraph(" ", quebraLinhaFonte);
        Phrase phrase;

        int countCity=0;

        for(int i =0;i<bandeiras.size();i++){

            if (i == 0){
                document.add(quebraLinha);
                document.add(quebraLinha);
            }

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            table.setWidths(new int[]{1, 6});
            Bitmap bitmap = BitmapFactory.decodeResource(ctx.getResources(), bandeiras.get(i).getIcone());

            table.addCell(createImageCell(getPathImage(bitmap, ctx)));


            NORMAL =  new Font(big);
            NORMAL.setStyle(Font.BOLD);
            NORMAL.setSize(14);

            table.addCell(createTextCell("   "+bandeiras.get(i).getBandeira().toUpperCase(),NORMAL));

            phrase = new Phrase();
            phrase.add(table);
            document.add(phrase);
            document.add(quebraLinha);

            for(DocumentoCartorio aux:documento.getDocumentoCartorios()){
                String codigoUFCidade = String.valueOf(aux.getCodigoCidade()).substring(0,2);

                if (bandeiras.get(i).getId() == Integer.parseInt(codigoUFCidade) && tipoConsulta == 1
                        || tipoConsulta == 2 && codigoCidade == aux.getCodigoCidade()) {
                    countCity++;
                    List list = new List(List.UNORDERED);
                    list.setListSymbol("");
                    //Setando a fonte de cidade como negrito
                    BaseFont urName = BaseFont.createFont("assets/fonts/big.ttf", "UTF-8",BaseFont.EMBEDDED);
                    Font urFontName = new Font(urName, 12);
                    NORMAL =  new Font(urFontName);
                    NORMAL.setStyle(Font.BOLD);
                    NORMAL.setSize(10);

                    ListItem item = new ListItem(new Paragraph("CIDADE: "+aux.getCidade(), NORMAL));
                    item.setAlignment(Element.ALIGN_JUSTIFIED);
                    list.add(item);

                    NORMAL =  new Font(urFontName);
                    NORMAL.setSize(8);
                    NORMAL.setStyle(Font.NORMAL);

                    item=new ListItem(new Paragraph("CARTORIO: "+aux.getNome(),NORMAL));
                    item.setAlignment(Element.ALIGN_JUSTIFIED);
                    list.add(item);

                    item=new ListItem(new Paragraph("ENDEREÇO: "+aux.getEndereco(),NORMAL));
                    item.setAlignment(Element.ALIGN_JUSTIFIED);
                    list.add(item);
                    item=new ListItem(new Paragraph("TELEFONE: "+aux.getTelefone(),NORMAL));
                    item.setAlignment(Element.ALIGN_JUSTIFIED);
                    list.add(item);
                    item=new ListItem(new Paragraph("QTD.PROTESTOS: "+aux.getQtdProtestos(),NORMAL));
                    item.setAlignment(Element.ALIGN_JUSTIFIED);
                    list.add(item);
                    item=new ListItem(new Paragraph("DATA ATUALIZAÇÃO: "+ClasseUtil.convertDateToString(aux.getDtAtualizacao()),NORMAL));
                    item.setAlignment(Element.ALIGN_JUSTIFIED);
                    item.setFont(NORMAL);
                    list.add(item);
                    phrase = new Phrase();
                    phrase.add(list);
                    document.add(phrase);

                    if(countCity==6  ){
                        document.newPage();
                        countCity=0;
                        document.add(quebraLinha);
                        document.add(quebraLinha);

                        PDFEventListenterRelatorio.pageNumber ++;
                    }

                    document.add(quebraLinha);
                }
            }
        }
    }

    private String getPathImage(Bitmap bitMap, Context ctx) {

        String fileName = "estados.jpg";
        String mPath = null;
        try {

            FileOutputStream out1 = ctx.openFileOutput(fileName, Context.MODE_PRIVATE);
            bitMap.compress(Bitmap.CompressFormat.JPEG, 100, out1);
            out1.flush();
            out1.close();
            File f = ctx.getFileStreamPath(fileName);
            mPath = f.getAbsolutePath();
            Log.i("PATH image", mPath);

        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return mPath;
    }

    public static PdfPCell createImageCell(String path) throws DocumentException, IOException {
        Image img = Image.getInstance(path);
        img.scaleAbsolute(30, 40);
        PdfPCell cell = new PdfPCell(img, true);
        return cell;
    }

    public static PdfPCell createTextCell(String text,Font font) throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell();
        Paragraph p = new Paragraph(text,font);
        p.setAlignment(Element.ALIGN_LEFT);
        cell.addElement(p);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }
    public void manipulatePdf(String src, String dest) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        int n = reader.getNumberOfPages();
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PdfContentByte pagecontent;
        for (int i = 0; i < n; ) {
            pagecontent = stamper.getOverContent(++i);
            ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                    new Phrase(String.format("Página  %s de %s", i, n)), 559, 806, 0);
        }
        stamper.close();
        reader.close();
    }
}
