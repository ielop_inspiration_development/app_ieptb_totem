package ieptb.ielop.br.com.ieptb.data;

import java.util.ArrayList;

import ieptb.ielop.br.com.ieptb.R;
import ieptb.ielop.br.com.ieptb.entity.Bandeira;

/**
 * Created by Saulo G BIttencourt on 18/05/2016.
 */
public class BandeiraData {

    static int id[] = {11, 12, 13, 14, 15, 16,
                       17, 21, 22, 23, 24, 25,
                       26, 27, 28, 29, 31, 32,
                       33, 35, 41, 42, 43, 50,
                       51, 52, 53};

    static String nome[] = {"Rondônia","Acre","Amazonas","Roraima","Pará",
                            "Amapá","Tocantins","Maranhão","Piauí","Ceará",
                            "Rio Grande do Norte","Paraíba","Pernambuco", "Alagoas","Sergipe",
                            "Bahia","Minas Gerais","Espírito Santo","Rio de Janeiro","São Paulo",
                            "Paraná","Santa Catarina","Rio Grande do Sul","Mato Grosso do Sul","Mato Grosso",
                            "Goiás","Distrito Federal"};
    static int icone[]={R.drawable.rondonia,R.drawable.acre,R.drawable.amazonas,R.drawable.roraima,R.drawable.para,
                        R.drawable.amapa,R.drawable.tocantins,R.drawable.maranhao,R.drawable.piaui,
                        R.drawable.ceara,R.drawable.rio_grande_do_norte,R.drawable.paraiba,R.drawable.pernambuco,
                        R.drawable.alagoas,R.drawable.sergipe,R.drawable.bahia,R.drawable.minas_gerais,R.drawable.espirito_santo,
                        R.drawable.rio_de_janeiro,R.drawable.sao_paulo,R.drawable.parana,R.drawable.santa_catarina,
                        R.drawable.rio_grande_do_sul,R.drawable.mato_grosso_do_sul,R.drawable.mato_grosso,R.drawable.goias,R.drawable.distrito_federal};

    public static ArrayList<Bandeira> getBandeiras(){
        ArrayList<Bandeira> bandeiras = new ArrayList<>();

        for (int i = 0; i < nome.length; i++) {
            Bandeira bandeira = new Bandeira(id[i],nome[i],icone[i]);
            bandeiras.add(bandeira);
        }

        return bandeiras;
    }
}
